# Cheese Rolls

## Ingredients

### Dough

* Water 270 ml (warm water, not hot or boiling)
* Oil 1 tbsp

* Sugar 2 tbsp
* Salt 1 tsp
* White Bread Flour 3 cups
* Instant Yeast 1 tsp

### Toppings

* 350 g cheddar cheese gratted

## Method

### Making the dough

Note: Makes 10 - 12 round rolls

* Sift dry ingredients (excluding the toppings)
* Slowly mix in the oil and water
* Mix with knife until mixture starts forming a dough ball
* Kneed the dough and add additional flour or water if needed
* Kneed for 10-15 minutes
* Dough should be pliable but not sticky
* Cover bowl with cling wrap and dry cloth for 0:45 to 1:00 hours until dough doubles in size
* Kneed for 5 minutes
* Cover bowl with cling wrap and dry cloth for 0:45 to 1:00 hours until dough doubles in size

### Making the rolls

* Pre-heat oven to 190°C (375°F)
* Divide the dough into 8-10 balls (10-12 balls for round rolls)
* Use spray and cook on baking tray
* Place dough balls on tray and flatten slightly
* Sprinkle dough balls generously with grated cheese
* Bake for 23 minutes