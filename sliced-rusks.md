# Sliced Rusks

## Dry Ingredients

* 4 1/2 cups flour
* 6 teaspoons baking powder
* 2 teaspoons bicarb
* 1 teaspoons salt

## Wet Ingredients

* 320ml plain double-cream yogurt
* 1 eggs (large)
* 1 cups white sugar
* 250g butter
* 2 teaspoons vanilla extract

## Alternative 1

* Add 1/2 cup coaco powder

## Alternative 2

* 400ml Coconut Cream 
* 3/4 cups flour

## Method

* Preheat oven to 400° F
* Melt butter in microwave
* Add other wet ingredients and beat well
* Sift and add dry ingredients
* Mix until mixture turns into a thick dough, add more flour if the dough is too sticky
* Add dough to two well greased glass dishes
* Bake for 35 minutes
* Allow to cool fully
* Slice into rusks
* Dry rusks in 400° F oven for 3 hours. 
* Turn off oven and leave overnight
* When the rusks are dry, store them in an airtight container