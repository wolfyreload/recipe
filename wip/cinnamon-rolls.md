# Cinnamon Rolls

## Dough

* 2 teaspoon active dry yeast
* 1 cup warm whole milk (45° to 50°C)
* 1/2 cup sugar
* 80 ml oil
* 2 large eggs
* 1 teaspoon salt
* 4 1/2 cups all-purpose flour

## Filling

* 3/4 cup packed sugar (preferably brown sugar)
* 2 tablespoons ground cinnamon
* 8 tablspoons oil

## Instructions

* Dissolve the yeast in warm milk. After a few minutes, it should be frothy and ready to use.
* In a stand mixer bowl, combine sugar, oil, eggs and salt with the yeast mixture and 2 cups flour; beat on medium speed until smooth. 
* Stir in enough remaining flour to form a soft dough.
* Turn dough onto a floured surface and knead until smooth and elastic, about 6-8 minutes.
* Let rise in a warm place until doubled; it will take about an hour.
* Mix brown sugar and cinnamon. Punch down the dough and divide it in half.
* On a lightly floured surface, roll one portion of dough into an 11×8-inch rectangle. Brush with 4 tablespoons of oil, then sprinkle with half of the brown sugar mixture to within 1/2 inch of the edges.
* Tightly roll the dough and cut into 8 portions
* Repeat with second portion of dough
* Place on baking tray and allow the cinnimon rolls 1 hour to double in size
* Preheat the oven to 180°C (350°F).
* Bake for 25 minute
* Allow to cool
* Reheat before serving

Note: Try with just having a Cinnamon roll loaf rather than individual rolls as making the rolls is messy. Rather cut after baking