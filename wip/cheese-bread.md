Cheese Bread
===================

Dry Ingredients
-------------------
* 1,000 mg bread flour
* 10 g active dry yeast
* 2 Tsp. sugar
* 2 tsp. salt
* 500 ml grated cheese
* 200 ml thinly sliced cheese
* 1 packet finely chopped meat

Wet Ingredients
--------------------
* 4 Tsp. oil
* 250 ml milk or water
* 60 ml warm water
* 1 egg (optional)

Method
--------------------
* Heat 250 ml milk or water for 2.5 minutes on high
* Add 60 ml warm water. Set aside until lukewarm.
* Sift add dry ingredients (including the grated cheese and meat but not the sliced cheese) then add liquid (and an egg, if desired)
* Mix well
* Leave for 10 to 15 minutes
Knead well for 3-5 minutes, until dough is soft and pliable. Place in well-greased bowl, cover and leave in warm place until doubled in size.
* Knead lightly and shape into a loaf. Place in a greased baking tray, cover and leave in a warm place until doubled in size.
* Bake in the over at 400F for 35-40 minutes
