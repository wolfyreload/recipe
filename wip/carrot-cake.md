Carrot-Pineapple Cake
========================

Dry Ingredients
------------------------
* 500 ml flour
* 350 ml sugar
* 5ml baking powder
* 5ml tsp soda
* 3ml salt
* 5ml cinnamon

Wet Ingredients
------------------------
* 250 ml salad oil
* 2 Eggs
* 1 Tsp mayonnaise
* 150 ml crushed pineapple + syrup
* 5 ml vanilla essence
* 1 large grated carrot

Method
-----------------------
* Sift all dry ingredients
* add wet ingredients in order above
* Bake for 40 minutes (or more) at 350&deg;F
