# Condense Milktart

## Instruments

* Electric mixer
* 30cm x 20cm glass dish
* Stove safe pot
* Spray and cook

## List 1

* 300 ml milk
* 4 eggs large
* 12 tablespoons cornflour

## List 2

* 1 tin condensed milk
* 1000 ml milk
* 2 heaped teaspoons margarine
* 3 teaspoons vanilla extract

## Other ingredients

* 1 Packet of Tennis Biscuits
* cinnamon ground
* nutmeg ground

## Instructions

* Spray glass dish with spray and cook
* Place layer of Tennis Biscuits on bottom of glass dish
* Beat ingredients in List 1 until smooth
* Place ingredients of List 2 into stove safe pot and put stove on max
* Once mixture starts to boil, take pot off heat and stir in the ingredients of List 1
* Stir until mixture thickens, mixure should be thick like cream cheese
* Add mixure to glass dish and smooth filling
* Add cinnamon and a dash of nutmeg 
* Allow to cool for 2 hours
* Cover with plastic film and refigerate for at least 2 hours