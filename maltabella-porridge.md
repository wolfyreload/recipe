# Maltabella Porridge

## Ingredients

* 1 cup Maltabella
* 250 ml cold water
* 1 pinch of salt
* 450 ml hot milk
* 8 tsp suger

## Steps

* Add Maltabella to pot
* Add pinch of salt
* Add cold water and make paste
* Boil milk in microwave for 2 minutes
* Add boiling milk
* Heat on stove at max heat and stir constantly
* Turn down heat to 7 as mixture boils or thickens
* Stir until desired thickness
* Add suger and serve hot