# Milo Muffins #

Makes 10-12 muffins

## Dry Ingredients ##

* 1 1/2 cups all-purpose flour
* 1/2 cup + 2 tablespoons Milo powder (chocolate malt powder)
* 1/2 cup granulated sugar
* 2 teaspoons baking powder
* 2 ml salt

## Wet Ingedients ##

* 125 ml milk
* 60 ml vegetable oil
* 1 large egg
* 1 teaspoon vanilla extract

## Instructions ##

1. Sift dry ingredients together and keep sifted out milo lumps for topping

2. Add wet ingredients together and beat until smooth

3. Add dry ingredients to wet ingredients and stir until just combined

4. Use spray and cook or grease 12 microwave safe muffin cups

5. Add batter to cups and fill each cup to maximum two-thirds full.

6. Add some milo lumps to each muffin

7. Microwave on high for 2:20 minutes. Check the muffins for doneness by inserting a toothpick into the center of one muffin. If it comes out clean or with a few crumbs, they are done. If not, continue microwaving in 30-second intervals until they are fully cooked.

8. Allow 15-20 minutes for muffins to cool and remove from muffin cups
