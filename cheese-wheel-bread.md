# Cheese Wheel Bread

## Ingredients

### Dough

[Same As Cheese rolls](./cheese-rolls.md)

### Toppings

* 350 g cheddar cheese gratted
* 4 strips of bacon

## Method

### Making the dough

[Same As Cheese rolls](./cheese-rolls.md)

### Prepping the bacon

* Cook the bacon between 2 paper plates for 2 minutes on high
* Cut bacon into small pieces

### Making the bread

* Pre-heat oven to 190°C (375°F)
* Sprink work surface with flour
* Roll out the dough until flat as if you were making a very large pizza
* Add gratted cheese and bacon evenly but leave at least a 1cm margin between the edge of the dough and the cheese
* Brush water along the edges of the dough margin
* Roll dough with cheese into a swiss-role shape
* Place on greesed baking tray
* Bake for 23 minutes