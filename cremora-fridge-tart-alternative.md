# Cremora Fridge Tart Alternative #

## Ingredients ##

* 500 g Boston Creamer with Palm Oil
* 500 ml milk
* 200 ml lemon juice
* 1 tin condensed milk
* 1 packet Tennis Biscuits

Method
--------------------------
* Put biscuits at bottom of a large rectangular casserole dish.
* Mix coffee creamer, milk and condensed milk and beat together until smooth.
* And lemon juice - mix well until thick.
* Pour onto biscuits
* Chill for at least 2 hours.
