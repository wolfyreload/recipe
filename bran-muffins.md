# Bran Muffins

## Dry Ingredients

* 3 cups wheat bran
* 2 cups cake flour
* 2 teaspoons baking powder
* 2 teaspoons bicarb
* 1 teaspoons salt

## Wet Ingredients

* 680ml plain double-cream yogurt
* 2 eggs (large)
* 150ml vegetable oil
* 1 1/2 cups of brown sugar
* 1 teaspoons vanilla extract

## Optional 1

* 1 cup raisins

## Method

* Preheat oven to 375° F
* Add cups to 24 muffin cups
* Beat **Wet Ingredients**
* Sift and add **Dry Ingredients** to **Wet Ingredients**
* Mix briefly until both mixed together
* Spoon batter into prepared muffin cups
* Bake for 17 minutes
