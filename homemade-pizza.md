# Homemade Pizza

## Ingredients

### Base 

[Same As Cheese rolls](./cheese-rolls.md)

### Toppings

* 1 tin braai relish (strained) or Wellington tomatoe sause
* Mozzarella Cheese (gratted) 350 g
* Ham 2 slices (cut into small squares)  
* Bacon 2 strips
* Origanum 20-25ml

## Method

### Making the base

[Same As Cheese rolls](./cheese-rolls.md)

### Prepping the Bacon

[Same as Cheese-wheel bread](./cheese-wheel-bread.md)

### Prepping the base, toppings and bake instructions

* Preheat oven to 200°C
* Split base in 2 (if you want a thin crust)
* Flatten on a sheet of flour
* Add to large baking tray (greased with spray and cook)
* Bake base for 10 minutes in 200°C oven

### The toppings

* Cover top of pizza base with thin layer of tomatoe sauce or braai relish
* Sprinkle Origanum on tomatoe base
* Add all of the cheese
* Add other toppings
* Bake in oven for 15 minutes
