# Mug Muffin #

## Dry Ingredients ##

* 1/4 cup all-purpose flour
* 2 tablespoons unsweetened cocoa powder
* 2 tablespoons suger
* 1/4 teaspoon baking powder

## Wet Ingedients ##

* 1/4 cup milk (room temperature)
* 2 tablespoons butter (melted and cooled)
* 1/4 teaspoon pure vanilla extract

optional

* 1 tablespoon creamy peanut butter
* 1 tablespoon mini chocolate chips

## Instructions ##

1. In a small bowl, whisk together flour, cocoa powder, sugar, and baking powder until thoroughly, completely combined with no steaks of flour or cocoa powder remaining. 

2. Blend in milk, butter and vanilla until batter is smooth.

3. Pour batter into a 14-ounce (or larger) microwave-safe mug with straight sides

4. Combine peanut butter and chocolate chips and dollop into the center of the mug. Gently pressing down until even with the top of the batter. Microwave on high for 1 minute. Allow to cool for a couple of minutes before serving.

## Notes ##

A tall, cylindrical mug with straight sides, will ensure that the cake bakes evenly.