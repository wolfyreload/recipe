Cremora Fridge Tart
==========================

Ingredients
--------------------------
* 500 g Cremora
* 500 ml milk
* 250 ml lemon juice
* 1 tin condensed milk
* 1 packet Tennis Biscuits (Preferably Bakers Lemon Tennis Biscuits)

Method
--------------------------
* Put biscuits at bottom of a large rectangular casserole dish.
* Mix Cremora, milk and condensed milk and beat together until smooth.
* And lemon juice - mix well until thick.
* Pour onto biscuits
* Chill for at least 2 hours.
