# Best Brownies

Original: <https://www.allrecipes.com/recipe/10549/best-brownies/>

## Ingredients

* 250 g butter (1/2 brick)
* 2 cups brown sugar
* 4 large eggs
* 2 teaspoons vanilla extract
* 2/3 cup unsweetened cocoa powder (2 measures of 1/3 cup)
* 1 cup cake flour
* 2ml salt
* 5ml baking powder

## Steps

* Preheat oven to 350°F (175°C)
* Use spray and cook on reactanglur glass dish (inner surface around 34cm x 23cm x 6cm)
* Melt butter for 1 minute in the microwave
* Add eggs, suger and vanilla and beat well.
* Sift dry ingedients together (flour, coaco, salt and baking powder)
* Add dry ingredients and mix with big spoon
* If consistency is not pourable add 20 ml of water
* Add batter to glass dish 
* Bake for 46 minutes (don't over cook)
* Allow to cool
* Cover and and serve after 5-6 hours
* Makes 24 servings