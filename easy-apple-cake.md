# Easy Apple Cake

## Dry Ingredients ##

* 2 cups all-purpose flour
* 1 teaspoons baking powder
* 1 teaspoon salt
* 1 teaspoon ground cinnamon
* 1 teaspoon baking soda

## Wet Ingedients ##

* 4 peeled and chopped granny smith apples
* 1 cup sugar
* 1 cup canola oil
* 3 large eggs
* 1 teaspoon vanilla extract

## Instructions ##

* Preheat oven to 350° F. 
* Heat chopped apples in microwave on high for 1.5 minutes
* In a large bowl, beat all wet ingredients except for the apples. 
* Combine the dry ingredients and fold in the apples
* Transfer to a greased 13x9-in baking dish. 
* Bake for 43 minutes
* Cool on a wire rack. 
* Serve with whipped topping.
