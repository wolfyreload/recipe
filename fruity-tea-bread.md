# Fruity Tea Bread

## Ingredients 1

* 2 Tea bags (5 Roses or Joko tea)
* 2 Cups mixed dried fruit
* 300 ml boiling water
* 1 1/4 Cups Brown Sugar

## Ingredients 2

* 2 1/2 Cups Cake Flour
* 1 Egg
* 4 tsp baking powder

## Method

* Add Tea bags to jug, add boiling water.
* Allow Tea to draw for 5 minutes and squeeze and remove tea bags
* Add sugar and mix well
* Add dried fruit and mix well
* Allow to cool, cover and leave overnight

* Preheat oven to 325°F (160°C)
* Grease 1KG loaf tin and line with baking paper
* Beat egg
* Sift flour and baking powder
* Add egg mix and tea mix and mix until smooth
* Add mixture to loaf tin 
* Bake for 01:45 hours